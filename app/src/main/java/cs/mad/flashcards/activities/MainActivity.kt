package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetsDB
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val setDao by lazy { FlashcardSetsDB.getDatabase(applicationContext).flashcardSetDao() }


     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.flashcardSetList.adapter = FlashcardSetAdapter(listOf(), setDao)

         binding.createSetButton.setOnClickListener {
                (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem()
            }


         loadFromDb()




    }

    private fun loadFromDb() {
        lifecycleScope.launch {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).setData(setDao.getAll())
        }
    }

    suspend fun createAddButton() {

    }
}