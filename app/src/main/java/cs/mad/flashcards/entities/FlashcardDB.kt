
package cs.mad.flashcards.entities

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Flashcard::class], version = 1)
abstract class FlashcardDB: RoomDatabase() {
    abstract fun flashcardDao(): FlashcardDao

    companion object {

        @Volatile
        private var INSTANCE: FlashcardDB? = null

        fun getDatabase(context: Context): FlashcardDB {


            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FlashcardDB::class.java,
                    "car_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

}