package cs.mad.flashcards.entities

import androidx.room.*

@Entity data class FlashcardSet(@PrimaryKey(autoGenerate = true) val id: Long?, val title: String)

@Dao
interface FlashcardSetDao {
    @Query("SELECT * FROM FlashcardSet")
    fun getAll(): List<FlashcardSet>

    @Insert
    fun insert(vararg set: FlashcardSet)

}

