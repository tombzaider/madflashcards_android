package cs.mad.flashcards.entities

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [FlashcardSet::class], version = 1)
abstract class FlashcardSetsDB: RoomDatabase() {
    abstract fun flashcardSetDao(): FlashcardSetDao

    companion object {

        @Volatile
        private var INSTANCE: FlashcardSetsDB? = null

        fun getDatabase(context: Context): FlashcardSetsDB {


            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FlashcardSetsDB::class.java,
                    "cardset_database"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

}