package cs.mad.flashcards.entities

import androidx.room.*


@Entity
data class Flashcard(
    @PrimaryKey(autoGenerate = true) val id: Long?
    ,
    var question: String,
    var answer: String
)

@Dao interface FlashcardDao {
    @Query("SELECT * FROM Flashcard")
    fun getAll(): List<Flashcard>

    @Insert fun insert(card :Flashcard)

    @Update fun update(card: Flashcard)

    @Delete fun delete(card: Flashcard)
}

fun getHardcodedFlashcards(): List<Flashcard> {
    return listOf()
}